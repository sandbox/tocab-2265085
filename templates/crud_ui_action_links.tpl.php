<?php
/**
 * @file
 * Theme the action links
 *
 * Available variables:
 * - $links: List of link.
 *
 * @ingroup themeable
 */

?>
<ul class="action-links">
  <?php foreach($links as $link): ?>
    <li>
      <?php if (is_array($link)): ?>
        <?php print l(
          $link['text'],
          $link['path'],
          empty($link['options']) ? array() : $link['options']
        ); ?>
      <?php else: ?>
        <?php print $link; ?>
      <?php endif; ?>
    </li>
  <?php endforeach; ?>
</ul>
