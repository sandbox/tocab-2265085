<?php
/**
 * @file
 * This file defines types than can be used with hook_crud_ui
 */

/**
 * Implements hook_crud_ui_api().
 */
function crud_ui_crud_ui_api() {
  return array(
    'types' => array(
      'float'        => array(
        'filter_form'     => 'crud_ui_range_filter_form',
        'filter_callback' => 'crud_ui_range_filter_callback',
        'validate'        => array('crud_ui_float_validate'),
      ),
      'numeric'      => array(
        'extends' => 'float',
      ),
      'int'          => array(
        'extends'  => 'float',
        'validate' => array('crud_ui_int_validate'),
      ),
      'serial'       => array(
        'extends'   => 'int',
        'disabled'  => TRUE,
        'submit'    => 'crud_ui_serial_submit',
      ),
      'varchar'      => array(),
      'char'         => array(),
      'text'         => array(),
      'blob'         => array(),
      'action_links' => array(
        'title'          => 'Action links',
        'fictive'        => TRUE,
        'display_list'   => 'crud_ui_actions_display_list_callback',
        'display_view'   => FALSE,
        'display_export' => FALSE,
        'list_theme'     => 'crud_ui_actions',
      ),
    ),

    'field_option_default_values' => array(
      'type'           => 'varchar',
      'order'          => 10,
      'serialize'      => FALSE,
      'display_list'   => TRUE,
      'display_add'    => TRUE,
      'display_view'   => TRUE,
      'display_edit'   => TRUE,
      'display_export' => TRUE,
      'fictive'        => FALSE,
      'filterable'     => TRUE,
      'sortable'       => TRUE,
      'required'       => FALSE,
      'disabled'       => FALSE,
      'unsigned'       => FALSE,
    ),
  );
}

/**
 * Filter form element for ranged fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return array
 *   Field form part
 */
function crud_ui_range_filter_form($field, $schema, $values) {
  $form = array();
  $form[$field . '-from'] = array(
    '#type'   => 'textfield',
    '#title'  => crud_ui_t('from :'),
    '#size'   => NULL,
    '#prefix' => '<div class="from-to">',
  );
  $form[$field . '-to'] = array(
    '#type'   => 'textfield',
    '#title'  => crud_ui_t('to :'),
    '#size'   => NULL,
    '#suffix' => '</div>',
  );

  // Set defaults values.
  if (!empty($values[$field . '-from'])) {
    $form[$field . '-from']['#default_value'] = $values[$field . '-from'];
  }
  if (!empty($values[$field . '-to'])) {
    $form[$field . '-to']['#default_value'] = $values[$field . '-to'];
  }

  return $form;
}

/**
 * Add the query condition for text fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   User values
 * @param SelectQuery $select
 *   Select query object
 */
function crud_ui_text_filter_callback($field, $schema, $values, $select) {
  if (!empty($values[$field])) {
    $select->condition(
      $schema['fields'][$field]['full_name'],
      '%' . $values[$field] . '%',
      'LIKE'
    );
  }
}

/**
 * Add the query condition for ranged fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   User values
 * @param SelectQuery $select
 *   Select query object
 */
function crud_ui_range_filter_callback($field, $schema, $values, $select) {
  $field_schema = $schema['fields'][$field];

  if (!empty($values[$field . '-from'])) {
    $select->condition(
      $field_schema['full_name'],
      $values[$field . '-from'],
      '>='
    );
  }
  if (!empty($values[$field . '-to'])) {
    $select->condition(
      $field_schema['full_name'],
      $values[$field . '-to'],
      '<='
    );
  }
}

/**
 * Submit function for serial fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return string
 *   The value to be stored in the field
 */
function crud_ui_serial_submit($field, $schema, $values) {
  return NULL;
}

/**
 * Check if the action col is available.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   CRUD UI schema
 *
 * @return bool
 *   Available or not
 */
function crud_ui_actions_display_list_callback($field, &$schema) {
  $schema['fields'][$field]['display_list']
    = ($schema['edit'] && crud_ui_access('edit', $schema['sid']))
    || ($schema['delete'] && crud_ui_access('delete', $schema['sid']));

  return $schema['fields'][$field]['display_list'];
}
