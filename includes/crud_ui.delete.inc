<?php
/**
 * @file
 * This contains the delete menu callbacks
 */

/**
 * Callback for menu.
 *
 * Delete a row for a table.
 *
 * @param array $sid
 *   The ID of the CRUD UI schema
 * @param string $key
 *   Serialized primary key
 *
 * @return string
 *   HTMl output
 */
function crud_ui_admin_delete($sid, $key) {
  try {
    $keys   = explode(',', $key);
    $schema = crud_ui_get_schema($sid);
    $query  = db_delete($schema['table']);
    foreach ($schema['primary key'][$schema['table']] as $id => $primary_key) {
      $query->condition($primary_key, $keys[$id]);
    }
    $query->execute();

    drupal_set_message(
      crud_ui_t('The row was successfully deleted'),
      'status'
    );
  }
  catch (Exception $e) {
    watchdog('crud_ui', $e->getMessage(), array(), WATCHDOG_ERROR);
    drupal_set_message($e->getMessage(), 'error');
  }

  drupal_goto(crud_ui_build_url_part($sid));
}
