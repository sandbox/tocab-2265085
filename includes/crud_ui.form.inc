<?php
/**
 * @file
 * This file contains the edit row form definition.
 */

/**
 * Implements hook_form().
 */
function crud_ui_row_form($form, &$form_state, $variables) {
  $schema = crud_ui_get_schema($variables['sid']);
  $cols   = array_keys($schema['fields']);

  $form                    = array();
  $form['#crud_ui_sid']    = $variables['sid'];
  $form['#crud_ui_action'] = $variables['action'];
  $form['#submit']         = array('crud_ui_row_form_submit');

  if ($variables['action'] == 'edit') {
    // Add hidden field with primary key.
    $keys = explode(',', $variables['key']);
    $form['crud_ui_pk'] = array(
      '#type'  => 'hidden',
      '#value' => $variables['key'],
    );

    // Fetch row with given key.
    $select = db_select($schema['table'], 'crud_ui_table')
      ->addTag('crud_ui')
      ->addTag('edit_form');
    $fields = array();
    foreach ($cols as $col) {
      $field_schema = $schema['fields'][$col];

      if (!$field_schema['fictive']
       && $field_schema['table'] == $schema['table']) {
        $fields[] = $col;
      }
    }
    foreach ($schema['primary key'][$schema['table']] as $id => $primary_key) {
      $select->condition($primary_key, $keys[$id]);
    }
    $record = $select->fields('crud_ui_table', $fields)
      ->execute()
      ->fetchAssoc();
  }

  $display = 'display_' . $variables['action'];

  foreach ($cols as $col) {
    $field_schema = $schema['fields'][$col];

    if (!$field_schema['fictive']
     && $field_schema['table'] == $schema['table']
     && (is_bool($field_schema[$display]) && $field_schema[$display])
      || (is_string($field_schema[$display]) && $field_schema[$display]($col, $schema))) {
      // Get default_value.
      $default_value = NULL;
      if ($variables['action'] == 'add' && !empty($field_schema['default'])) {
        $default_value = $field_schema['default'];
      }
      elseif ($variables['action'] == 'edit') {
        $default_value = $record[$col];
      }

      if (!empty($field_schema['field_form'])
       && function_exists($field_schema['field_form'])) {
        $form[$col] = $field_schema['field_form'](
          $col,
          $schema,
          $default_value
        );
      }
      else {
        $form[$col] = array(
          '#type'  => 'textfield',
        );
      }

      // Add and translate the title.
      $title = $col;
      if (!empty($field_schema['title'])) {
        $title = $field_schema['title'];
      }
      $form[$col]['#title'] = crud_ui_t($title);

      // Add and translate the description.
      if (!empty($field_schema['description'])) {
        $form[$col]['#description'] = crud_ui_t($field_schema['description']);
      }

      // Add element_validate.
      if (!empty($field_schema['validate'])) {
        $form[$col]['#element_validate'] = $field_schema['validate'];
      }

      // Add disabled.
      if (!empty($field_schema['disabled'])) {
        $form[$col]['#disabled'] = TRUE;
      }

      // Add required.
      if ($field_schema['required']
       || (!empty($field_schema['not null'])
        && !isset($field_schema['default']))) {
        $form[$col]['#required'] = TRUE;
      }

      // Add maxlength.
      if (!empty($field_schema['length'])) {
        $form[$col]['#maxlength'] = $field_schema['length'];
      }

      // Set default_value.
      $form[$col]['#default_value'] = $default_value;
    }
  }

  // Submit button.
  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => ($variables['action'] == 'add') ?
    crud_ui_t('Add') :
    crud_ui_t('Submit'),
  );

  return $form;
}

/**
 * Submit function for crud_ui_row_form().
 *
 * Save new row.
 *
 * @param array $form
 *   Form representation
 * @param array $form_state
 *   Form submissions
 */
function crud_ui_row_form_submit($form, &$form_state) {
  $schema = crud_ui_get_schema($form['#crud_ui_sid']);
  $cols   = array_keys($schema['fields']);
  $value  = NULL;
  $fields = array();

  try {
    foreach ($cols as $col) {
      $field_schema = $schema['fields'][$col];

      if (isset($form_state['values'][$col]) && !$field_schema['fictive']) {
        if (!empty($field_schema['submit'])
         && function_exists($field_schema['submit'])) {
          $value = $field_schema['submit'](
            $col,
            $schema,
            $form_state['values']
          );
        }
        else {
          $value = crud_ui_default_value(
            $col,
            $schema,
            $form_state['values']
          );
        }

        if ($value !== NULL) {
          $fields[$col] = $value;
        }
      }
    }

    if ($form['#crud_ui_action'] == 'add') {
      // Insert.
      db_insert($schema['table'])
        ->fields($fields)
        ->execute();

      drupal_set_message(
        crud_ui_t('The row was successfully inserted'),
        'status'
      );
    }
    elseif ($form['#crud_ui_action'] == 'edit') {
      // Update.
      $keys = explode(',', $form_state['values']['crud_ui_pk']);

      $query = db_update($schema['table']);
      foreach ($schema['primary key'][$schema['table']] as $id => $primary_key) {
        $query->condition($primary_key, $keys[$id]);
      }
      $query->fields($fields)->execute();

      drupal_set_message(
        crud_ui_t('The row was successfully updated'),
        'status'
      );
    }

    if ($form_state['redirect']) {
      drupal_goto($form_state['redirect']);
    }
    else {
      drupal_goto(crud_ui_build_url_part($schema['sid']));
    }
  }
  catch (Exception $e) {
    watchdog('crud_ui', $e->getMessage(), array(), WATCHDOG_ERROR);
    drupal_set_message($e->getMessage(), 'error');
  }
}
