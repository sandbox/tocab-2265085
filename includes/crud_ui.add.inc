<?php
/**
 * @file
 * This contains the add menu callbacks
 */

/**
 * Callback for menu.
 *
 * Display the add form for a table.
 *
 * @param array $sid
 *   The ID of the CRUD UI schema
 *
 * @return string
 *   HTMl output
 */
function crud_ui_admin_add($sid) {
  drupal_add_css(drupal_get_path('module', 'crud_ui') . '/css/crud_ui.css');

  $form = drupal_get_form(
    'crud_ui_row_form',
    array(
      'sid'    => $sid,
      'action' => 'add',
    )
  );

  return theme(
    'crud_ui_action_links',
    array(
      'links' => array(
        array(
          'text'    => crud_ui_t('Back'),
          'path'    => crud_ui_build_url_part($sid),
        ),
      ),
    )
  ) . render($form);
}
