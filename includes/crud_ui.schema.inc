<?php
/**
 * @file
 * This file build and return the CRUD UI schema
 */

/**
 * Build the CRUD UI schema for all tables and saved it into the cache.
 *
 * @return array
 *   The CRUD UI complete schema for all tables
 */
function crud_ui_build_schemas() {
  $complete_schemas = array();

  if (!($cache = cache_get('crud_ui_schemas'))) {
    // Call to hook_crud_ui_schema.
    $schemas = array();
    foreach (module_implements('crud_ui_schema') as $module) {
      $schemas[$module] = module_invoke($module, 'crud_ui_schema');
    }

    $schema_id = 0;
    foreach ($schemas as $module => $module_schemas) {
      foreach ($module_schemas as $options) {
        try {
          if (!empty($options['table']) && !empty($options['title'])) {
            // Add some informations.
            $options['sid'] = $schema_id;
            if (!isset($options['module'])) {
              $options['module'] = $module;
            }
            $hook_module = $options['module'];

            // Get the module schema.
            module_load_include('install', $hook_module);
            $schema = module_invoke($hook_module, 'schema');
            $schema = $schema[$options['table']];

            // Merge options with default values.
            $default_schema = crud_ui_get_default_schema_options();
            $options = array_merge(
                $default_schema,
                $options
            );

            // Add the 'primary key' option.
            $options['primary key'] = array(
              $options['table'] => $schema['primary key'],
            );

            // Merge the 'foreign keys' option.
            if (!empty($schema['foreign keys'])) {
              foreach ($schema['foreign keys'] as $key => $foreign) {
                if (isset($options['foreign keys'][$key])) {
                  $options['foreign keys'][$key] = array_merge(
                      $foreign,
                      $options['foreign keys'][$key]
                  );
                }
                else {
                  $options['foreign keys'][$key] = $foreign;
                }
              }
              $options['foreign keys'][$key]['linked_table'] = $options['table'];
              $options['foreign keys'][$key]['condition'] = crud_ui_build_condition($foreign, $options['table']);
            }

            // Push 'foreign keys' option into 'join' option.
            foreach ($options['foreign keys'] as $foreign) {
              $options['join'][] = $foreign;
            }

            // Look for table and field aliases.
            $aliases = array(
              'table' => array(),
              'alias' => array(),
              'field' => array(),
            );
            foreach ($options['join'] as $key => $table) {
              $aliases['table'][$table['table']]['key'] = $key;
              if (isset($table['alias'])) {
                $aliases['table'][$table['table']]['alias'] = $table['alias'];
                $aliases['alias'][$table['alias']] = $table['table'];
              }
              if (isset($table['module'])) {
                $aliases['table'][$table['table']]['module'] = $table['module'];
              }
            }
            foreach ($options['fields'] as $key => $field) {
              if (!empty($field['name']) && !empty($field['table'])) {
                if (isset($aliases['table'][$field['table']]['alias'])) {
                  $field['table'] = $aliases['table'][$field['table']]['alias'];
                }
                $aliases['field'][$field['table'] . '.' . $field['name']] = $key;
              }
            }
            $options['aliases'] = $aliases;

            // Get the schema of linked table.
            crud_ui_add_joined_schemas($schema, $options);

            // Add missing fields.
            foreach ($options['fields'] as $key => $field) {
              if (!isset($schema['fields'][$key])) {
                $schema['fields'][$key] = $field;
              }
            }

            // Merge fields and add default values.
            crud_ui_merge_fields($schema, $options);

            // Sort fields.
            uasort($options['fields'], 'crud_ui_schema_sort');

            $complete_schemas[$schema_id] = $options;
            $schema_id++;
          }
          elseif (empty($options['table'])) {
            drupal_set_message(
              'CRUD UI : ' . crud_ui_t(
                'The required option "table" for one of your schema declared in the module "%module" is missing.',
                array('%module' => $module)
              ),
              'warning'
            );
          }
          else {
            drupal_set_message(
              'CRUD UI : ' . crud_ui_t(
                'The required option "title" for the table "%table" declared in the "%module" module is missing.',
                array('%table' => $options['table'], '%module' => $module)
              ),
              'warning'
            );
          }
        }
        catch (Exception $e) {
          drupal_set_message(
            $e->getMessage(),
            'warning'
          );
        }
      }
    }

    drupal_alter('crud_ui_schema', $complete_schemas);
    cache_set('crud_ui_schemas', $complete_schemas, 'cache', CACHE_TEMPORARY);
  }
  else {
    $complete_schemas = $cache->data;
  }

  return $complete_schemas;
}

/**
 * Add joined table schema to the main schema.
 *
 * @param array $schema
 *   The base schema
 * @param array $options
 *   CRUD UI options
 */
function crud_ui_add_joined_schemas(&$schema, &$options) {
  $aliases = $options['aliases'];

  foreach ($options['join'] as $table_options) {
    if (isset($table_options['module'])) {
      // Load the schema.
      module_load_include('install', $table_options['module']);
      $joined_schema = module_invoke($table_options['module'], 'schema');
      $joined_schema = $joined_schema[$table_options['table']];

      // Store the table name.
      if (isset($table_options['alias'])) {
        $table = $table_options['alias'];
      }
      else {
        $table = $table_options['table'];
      }

      foreach ($joined_schema['fields'] as $key => $fields) {
        $alias = $key;

        // Look after an existing alias.
        if (array_key_exists($table . '.' . $key, $aliases['field'])) {
          $alias = $aliases['field'][$table . '.' . $key];
        }
        elseif (array_key_exists($key, $schema['fields'])) {
          // In that case we have fields we the same name and no field alias
          // defined => create the alias automatically.
          $alias = $table . '_' . $key;
          $joined_schema['fields'][$key]['name'] = $key;
        }

        // Add the foreign key declaration if necessary.
        foreach ($joined_schema['foreign keys'] as $foreign_key) {
          if (isset($aliases['table'][$foreign_key['table']])) {
            $foreign_key['linked_table'] = $table;
            $options['join'][$aliases['table'][$foreign_key['table']]['key']] = array_merge(
              $foreign_key,
              $options['join'][$aliases['table'][$foreign_key['table']]['key']]
            );
          }
        }

        // Update the primary key if necessary.
        $index = array_search($key, $joined_schema['primary key']);
        if ($index !== FALSE) {
          $joined_schema['primary key'][$index] = $alias;
        }

        // Insert the field into the schema.
        $joined_schema['fields'][$key]['table'] = $table;
        $schema['fields'][$alias] = $joined_schema['fields'][$key];
      }

      // Add the 'primary key' info for the table.
      $options['primary key'][$table] = $joined_schema['primary key'];
    }
  }

  // Generates the condition for new foreign keys.
  foreach ($options['join'] as $key => $foreign) {
    if (empty($options['join'][$key]['condition'])) {
      $table = $options['table'];
      if (isset($foreign['linked_table'])) {
        $table = $foreign['linked_table'];
      }
      $options['join'][$key]['condition'] = crud_ui_build_condition($foreign, $table);
    }
  }
}

/**
 * Merge fields with default, schema and option values.
 *
 * @param array $schema
 *   The base schema
 * @param array $options
 *   CRUD UI options
 */
function crud_ui_merge_fields($schema, &$options) {
  // Get types.
  $types = crud_ui_get_types();

  // Initialize default field options.
  $default_field = crud_ui_get_field_option_default_values();
  $default_field['order'] = 10;

  foreach ($schema['fields'] as $key => $field) {
    // Merge with default options.
    $field = array_merge(
      $default_field,
      $field
    );

    // $original_type = $field['type'];
    // Get the field type.
    $type = $field['type'];
    if (isset($options['fields'][$key])
     && isset($options['fields'][$key]['type'])) {
      $type = $options['fields'][$key]['type'];
    }

    // Merge the field with the type options.
    if (isset($types[$type])) {
      $field = array_merge(
        $field,
        $types[$type]
      );
    }

    // Override with CRUD UI options.
    if (isset($options['fields'][$key])) {
      $field = array_merge(
        $field,
        $options['fields'][$key]
      );
    }

    // Increment the default order.
    $default_field['order'] += 10;

    // Set the real table name.
    if (empty($field['table'])) {
      $field['table'] = $options['table'];
    }
    elseif (isset($options['aliases']['alias'][$field['table']])) {
      $field['table'] = $options['aliases']['alias'][$field['table']];
    }

    // Set the table alias.
    if (isset($options['aliases']['table'][$field['table']]['alias'])) {
      $field['alias'] = $options['aliases']['table'][$field['table']]['alias'];
    }
    else {
      $field['alias'] = $field['table'];
    }

    // Set the field names.
    if (empty($field['name'])) {
      $field['name'] = $key;
    }
    $field['full_name'] = $field['alias'] . '.' . $field['name'];

    // Does the field implements a schema callback ?
    if (!empty($field['schema_callback'])
        && function_exists($field['schema_callback'])) {
      $field['schema_callback']($key, $field);
    }

    $options['fields'][$key] = $field;
  }
}

/**
 * Get the CRUD UI API informations and save them into the cache.
 *
 * @return array
 *   The list of defined types.
 */
function crud_ui_build_api() {
  $data = array(
    'types'                       => array(),
    'field_option_default_values' => array(),
  );

  if (!($cache = cache_get('crud_ui_api'))) {
    // Call to hook_crud_ui_api.
    foreach (module_implements('crud_ui_api') as $module) {
      $api = module_invoke($module, 'crud_ui_api');

      foreach (array_keys($data) as $key) {
        if (!empty($api[$key])) {
          $data[$key] = array_merge($data[$key], $api[$key]);
        }
      }
    }

    // Manage 'extends' type options.
    foreach ($data['types'] as $type => $options) {
      if (array_key_exists('extends', $options)
       && isset($data['types'][$options['extends']])) {
        $data['types'][$type] = array_merge(
          $data['types'][$options['extends']],
          $options
        );
      }
    }

    cache_set('crud_ui_api', $data, 'cache', CACHE_TEMPORARY);
  }
  else {
    $data = $cache->data;
  }

  return $data;
}

/**
 * Get the CRUD UI types.
 *
 * @return array
 *   The list of defined types.
 */
function crud_ui_get_types() {
  $types = &drupal_static(__FUNCTION__);

  if (!isset($types)) {
    $api   = crud_ui_build_api();
    $types = $api['types'];
  }

  return $types;
}

/**
 * Get the default field options.
 *
 * @return array
 *   Default field options
 */
function crud_ui_get_field_option_default_values() {
  $values = &drupal_static(__FUNCTION__);

  if (!isset($values)) {
    $api    = crud_ui_build_api();
    $values = $api['field_option_default_values'];
  }

  return $values;
}

/**
 * Get the default schema options.
 *
 * @return array
 *   Default schema options
 */
function crud_ui_get_default_schema_options() {
  return array(
    'add'                => TRUE,
    'view'               => TRUE,
    'edit'               => TRUE,
    'delete'             => TRUE,
    'export'             => TRUE,
    'export_enclosure'   => '"',
    'export_delimiter'   => ',',
    'display_table_name' => FALSE,
    'table_attributes'   => array(),
    'fields'             => array(),
    'foreign keys'       => array(),
    'join'               => array(),
  );
}

/**
 * Sort the schema fields.
 *
 * @param string $a
 *   Field a to compare
 * @param string $b
 *   Field b to compare
 */
function crud_ui_schema_sort($a, $b) {
  return ($a['order'] == $b['order']) ?
    0 :
    (($a['order'] > $b['order']) ? 1 : -1);
}

/**
 * Build foreign table condition.
 *
 * @param array $foreign
 *   Foreign table information
 * @param string $table
 *   Table name
 *
 * @return string
 *   Builded condition
 */
function crud_ui_build_condition($foreign, $table) {
  $condition = '';
  foreach ($foreign['columns'] as $table_field => $foreign_field) {
    $condition .= $table .
    '.' . $table_field .
    ' = ' . $foreign['table'] .
    '.' . $foreign_field .
    ' && ';
  }
  return substr($condition, 0, -4);
}
