<?php
/**
 * @file
 * This contains themes
 */

/**
 * Theme for the crud_ui_form form.
 *
 * @param array $variables
 *   Array of variables : contains the form
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_form($variables) {
  $form      = $variables['form'];
  $schema    = crud_ui_get_schema($form['#crud_ui_sid']);
  $rows      = crud_ui_render_rows($form);
  $colgroups = crud_ui_render_colgroups($form);
  $table     = theme(
    'table',
    array(
      'header'     => $form['#header'],
      'rows'       => $rows,
      'sticky'     => TRUE,
      'colgroups'  => $colgroups,
      'empty'      => crud_ui_t('No records have been found.'),
      'attributes' => $schema['table_attributes'],
    )
  );

  // Render the page.
  $output  = render($form['crud-ui-limit']);
  $output .= render($form['submit']);
  $output .= render($form['reset']);
  $output .= render($form['export']);
  $output .= render($form['add']);
  $output .= theme(
    'html_tag',
    array(
      'element' => array(
        '#tag'        => 'div',
        '#value'      => $table,
        '#attributes' => array('class' => 'crui-ui-table-wrapper'),
      ),
    )
  );
  $output .= theme('pager');
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Theme for the text row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_text($variables) {
  $col = $variables['col'];
  $output = $variables['row']->$col;

  if (!empty($variables['schema']['fields'][$col]['serialize'])) {
    $infos  = unserialize($variables['row']->$col);
    $output = crud_ui_format_serialize_recursive($infos);
  }

  return $output;
}

/**
 * Recursively format output for an unserialized input.
 *
 * @param string $unserialized_value
 *   Unserialized value of unknown type to be displayed
 *
 * @return string
 *   HTML output
 */
function crud_ui_format_serialize_recursive($unserialized_value) {
  $output = '';

  switch (gettype($unserialized_value)) {
    case 'object':
      $output = ' OBJECT';
      $unserialized_value = get_object_vars($unserialized_value);
    case 'array':
      if (empty($output)) {
        $output = 'ARRAY';
      }
      $output .= '<ul>';
      foreach ($unserialized_value as $key => $value) {
        $output .= '<li>[' . $key . '] => ' . crud_ui_format_serialize_recursive($value) . '</li>';
      }
      $output .= '</ul>';
      break;

    case 'boolean':
      $output = $unserialized_value ? 'true' : 'false';
      break;

    case 'integer':
    case 'double':
    case 'string':
      $output = $unserialized_value;
      break;

    default:
      $output = strtoupper(gettype($unserialized_value));
      break;
  }

  return $output;
}

/**
 * Theme for the action links row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_actions($variables) {
  // Build uniq key for the main table.
  $primary_key = array();
  foreach ($variables['schema']['primary key'][$variables['schema']['table']] as $key) {
    $primary_key[] = $variables['row']->$key;
  }
  $primary_key = implode(',', $primary_key);

  $action_links = '';
  if ($variables['schema']['view'] && crud_ui_access('view', $variables['schema']['sid'])) {
    $action_links .= l(
        crud_ui_t('View'),
        crud_ui_build_url_part($variables['schema']['sid']) . '/view/' . $primary_key,
        array('attributes' => array('class' => 'action-link'))
    );
  }
  if ($variables['schema']['edit'] && crud_ui_access('edit', $variables['schema']['sid'])) {
    $action_links .= l(
      crud_ui_t('Edit'),
      crud_ui_build_url_part($variables['schema']['sid']) . '/edit/' . $primary_key,
      array('attributes' => array('class' => 'action-link'))
    );
  }
  if ($variables['schema']['delete'] && crud_ui_access('delete', $variables['schema']['sid'])) {
    $action_links .= l(
      crud_ui_t('Delete'),
      crud_ui_build_url_part($variables['schema']['sid']) . '/delete/' . $primary_key,
      array(
        'attributes' => array(
          'class'   => 'action-link',
          'onclick' => 'return window.confirm("' .
          crud_ui_t('Are you sure you want to delete this row ?') .
          '")',
        ),
      )
    );
  }
  return $action_links;
}
