<?php
/**
 * @file
 * This file contains the admin list view
 */

/**
 * Callback for menu.
 *
 * Display the CRUD UI list view for a given table.
 *
 * @param array $sid
 *   The ID of the CRUD UI schema
 *
 * @return string
 *   HTMl output
 */
function crud_ui_admin_list($sid) {
  drupal_add_css(drupal_get_path('module', 'crud_ui') . '/css/crud_ui.css');

  $form = drupal_get_form(
      'crud_ui_form',
      array('sid' => $sid)
  );
  $output = render($form);

  $schema = crud_ui_get_schema($sid);
  if (!empty($schema['theme'])) {
    $output = theme($schema['theme'], array('crud_ui' => $output));
  }

  return $output;
}

/**
 * Implements hook_form().
 */
function crud_ui_form($form, &$form_state, $variables) {
  foreach (module_implements('crud_ui_api') as $module) {
    module_load_include('inc', $module, $module . '.crud_ui');
  }

  $schema = crud_ui_get_schema($variables['sid']);
  $cols   = array_keys($schema['fields']);

  $form                  = array();
  $form['#theme']        = 'crud_ui_form';
  $form['#crud_ui_sid']  = $variables['sid'];
  $form['#crud_ui_cols'] = $cols;

  // Submit button.
  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => crud_ui_t('Filter'),
    '#submit' => array('crud_ui_form_submit'),
  );

  // Reset button.
  $form['reset'] = array(
    '#type'  => 'submit',
    '#value' => crud_ui_t('Reset'),
    '#submit' => array('crud_ui_form_reset'),
  );

  // Export button.
  if ($schema['export']) {
    $form['export'] = array(
      '#type'  => 'submit',
      '#value' => crud_ui_t('Export'),
      '#submit' => array('crud_ui_form_export'),
    );
  }

  // Page limit selector.
  if (!empty($schema['pager_options'])) {
    $options = $schema['pager_options'];
  }
  else {
    $options = array(
      '10'  => 10,
      '25'  => 25,
      '50'  => 50,
      '100' => 100,
      'all' => crud_ui_t('All'),
    );
  }
  $form['crud-ui-limit'] = array(
    '#type'    => 'select',
    '#title'   => crud_ui_t('Elements per page'),
    '#options' => $options,
  );

  // Initialize the query (without setting the pager limit).
  $pager_query = crud_ui_form_query($form);

  // Add filters conditions to the pager query.
  $full_query = clone($pager_query);
  crud_ui_form_conditions($form, $pager_query);

  // Find the pager limit.
  if (!empty($_SESSION['crud_ui'][$variables['sid']]['crud-ui-limit'])) {
    $limit = $_SESSION['crud_ui'][$variables['sid']]['crud-ui-limit'];
  }
  elseif (!empty($schema['pager_default'])
       && isset($options[$schema['pager_default']])) {
    $limit = $schema['pager_default'];
  }
  else {
    $limit = current($options);
  }

  // Apply the limit (only to the pager query).
  $form['crud-ui-limit']['#default_value'] = $limit;
  if (intval($limit) != 0) {
    $pager_query->limit(intval($limit));
  }

  // The pager query must be executed first for the pager to work.
  $results = $pager_query->execute()->fetchAll();
  $form['#crud_ui_results'] = $results;

  // Build rows.
  crud_ui_form_rows($form);

  // Then execute the full query (used for select filters, export...etc.).
  // We must set an arbitrary big value for the limit because 0, false or null
  // does not really desactivate the pager for PagerDefault query...
  // @see http://api.drupal.org/api/drupal/includes!pager.inc/function/PagerDefault%3A%3Alimit/7
  $form['#crud_ui_results'] = $full_query
    ->limit(2147483647)
    ->execute()
    ->fetchAll();

  // Build filters.
  crud_ui_form_filters($form);

  return $form;
}

/**
 * Initialize the select query.
 *
 * @param array $form
 *   Form representation
 *
 * return SelectQuery
 *  Select query
 */
function crud_ui_form_query(&$form) {
  $schema = crud_ui_get_schema($form['#crud_ui_sid']);
  $header = array();

  // Build header.
  foreach ($form['#crud_ui_cols'] as $col) {
    $field_schema = $schema['fields'][$col];

    if ((is_bool($field_schema['display_list']) && $field_schema['display_list'])
    || (is_string($field_schema['display_list']) && $field_schema['display_list']($col, $schema))) {
      $title = $col;
      if (!empty($field_schema['title'])) {
        $title = $field_schema['title'];
      }

      $header[$col] = array(
        'data' => $title,
      );

      if (!$field_schema['fictive']
       && $field_schema['sortable']
       && $field_schema['type'] != 'file') {
        $header[$col]['field'] = $col;

        // Add default sort if any.
        if (isset($schema['default_sort_field'])
         && $schema['default_sort_field'] == $col) {
          $sort = 'asc';
          if (isset($schema['default_sort_direction'])) {
            $default_sort_direction = strtolower($schema['default_sort_direction']);
            if (in_array($default_sort_direction, array('asc', 'desc'))) {
              $sort = $default_sort_direction;
            }
          }
          $header[$col]['sort'] = $sort;
        }
      }
    }
  }
  $form['#header'] = $header;

  return crud_ui_buid_query($schema, $header);
}

/**
 * Add filters conditions.
 *
 * @param array $form
 *   Form representation
 * @param SelectQuery $query
 *   Select query
 */
function crud_ui_form_conditions(&$form, $query) {
  $schema = crud_ui_get_schema($form['#crud_ui_sid']);
  $values = array();

  if (isset($_SESSION['crud_ui'])
   && isset($_SESSION['crud_ui'][$form['#crud_ui_sid']])) {
    $values = $_SESSION['crud_ui'][$form['#crud_ui_sid']];
  }

  foreach ($form['#crud_ui_cols'] as $col) {
    $field_schema = $schema['fields'][$col];

    if (((is_bool($field_schema['display_list']) && $field_schema['display_list'])
      || (is_string($field_schema['display_list']) && $field_schema['display_list']($col, $schema)))
    && !$field_schema['fictive']) {

      if (!empty($field_schema['filter_callback'])
       && function_exists($field_schema['filter_callback'])) {
        $field_schema['filter_callback'](
          $col,
          $schema,
          $values,
          $query
        );
      }
      else {
        crud_ui_text_filter_callback(
          $col,
          $schema,
          $values,
          $query
        );
      }
    }
  }
}

/**
 * Build form filters.
 *
 * @param array $form
 *   Form representation
 */
function crud_ui_form_filters(&$form) {
  $schema = crud_ui_get_schema($form['#crud_ui_sid']);
  $values = array();

  if (isset($_SESSION['crud_ui'])
   && isset($_SESSION['crud_ui'][$form['#crud_ui_sid']])) {
    $values = $_SESSION['crud_ui'][$form['#crud_ui_sid']];
  }

  foreach ($form['#crud_ui_cols'] as $col) {
    $field_schema = $schema['fields'][$col];

    if ((is_bool($field_schema['display_list']) && $field_schema['display_list'])
    || (is_string($field_schema['display_list']) && $field_schema['display_list']($col, $schema))) {
      if ($field_schema['fictive'] || !$field_schema['filterable']) {
        $form[$col] = array(
          '#markup'  => '',
        );
      }
      elseif (!empty($field_schema['filter_form'])
       && function_exists($field_schema['filter_form'])) {
        $form[$col] = $field_schema['filter_form'](
          $col,
          $schema,
          $values
        );
      }
      else {
        $form[$col] = array(
          '#type'  => 'textfield',
          '#title' => '',
          '#size'   => NULL,
        );
      }

      // Add default value if not already set.
      if (isset($values[$col])
       && isset($form[$col])
       && empty($form[$col]['#default_value'])) {
        $form[$col]['#default_value'] = $values[$col];
      }
    }
  }
}

/**
 * Build form rows.
 *
 * @param array $form
 *   Form representation
 */
function crud_ui_form_rows(&$form) {
  $rows         = array();
  $schema       = crud_ui_get_schema($form['#crud_ui_sid']);

  // Initialize previous row array.
  $previous_rows = array();
  foreach ($schema['primary key'] as $table => $keys) {
    $previous_rows[$table] = array(
      'key' => '',
      'id'  => 0,
    );
  }

  // Build column list.
  $cols = array();
  foreach ($form['#crud_ui_cols'] as $col) {
    $field_schema = $schema['fields'][$col];

    if ((is_bool($field_schema['display_list']) && $field_schema['display_list'])
    || (is_string($field_schema['display_list']) && $field_schema['display_list']($col, $schema))) {
      $cols[] = $col;
    }
  }

  foreach ($form['#crud_ui_results'] as $id => $record) {
    $primary_keys = array();
    $skip         = FALSE;

    foreach ($schema['primary key'] as $table => $keys) {
      // Build uniq key for each tables.
      $primary_keys[$table] = array();
      foreach ($keys as $key) {
        $primary_keys[$table][] = $record->$key;
      }
      $primary_keys[$table] = implode(',', $primary_keys[$table]);

      // Check is the previous row has the same key (for row merging).
      if (!$skip && $previous_rows[$table]['key'] == $primary_keys[$table]) {
        // Increment the rowspan and remove cols from the current row.
        foreach ($rows[$previous_rows[$table]['id']] as $col => $row) {
          if ($table == $schema['fields'][$col]['alias']) {
            $rows[$previous_rows[$table]['id']][$col]['rowspan']++;
            $schema['fields'][$col]['display_list'] = FALSE;
          }
        }
      }
      else {
        // Add the rows to be displayed.
        foreach ($cols as $col) {
          if ($table == $schema['fields'][$col]['alias']) {
            $schema['fields'][$col]['display_list'] = TRUE;
          }
        }

        // And store the new previous row.
        $previous_rows[$table]['key'] = $primary_keys[$table];
        $previous_rows[$table]['id']  = $id;

        // Skip further merging.
        $skip = TRUE;
      }
    }

    // Build rows from query.
    $row = array();
    foreach ($cols as $col) {
      $field_schema = $schema['fields'][$col];

      if ((is_bool($field_schema['display_list']) && $field_schema['display_list'])
      || (is_string($field_schema['display_list']) && $field_schema['display_list']($col, $schema))) {
        $theme_vars = array(
          'col'    => $col,
          'row'    => $record,
          'schema' => $schema,
        );

        if (!empty($field_schema['list_theme'])) {
          $row[$col] = theme($field_schema['list_theme'], $theme_vars);
        }
        else {
          $row[$col] = theme('crud_ui_text', $theme_vars);
        }
      }

      if (isset($row[$col])) {
        $row[$col] = array(
          'data'    => $row[$col],
          'rowspan' => 1,
        );
      }
    }
    $rows[$id] = $row;
  }

  // Empty rows ?
  if (empty($rows)) {
    $rows['empty'] = array(
      array(
        'data'    => crud_ui_t('No records have been found.'),
        'colspan' => count($form['#crud_ui_cols']),
      ),
    );
  }
  $form['#crud_ui_rows'] = $rows;
}

/**
 * Submit function for crud_ui_form().
 *
 * Filter the results.
 *
 * @param array $form
 *   Form representation
 * @param array $form_state
 *   Form submissions
 */
function crud_ui_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    if (!in_array($key, array('op', 'form_build_id', 'form_token', 'form_id'))) {
      $_SESSION['crud_ui'][$form['#crud_ui_sid']][$key] = $value;
    }
  }
}

/**
 * Submit function for crud_ui_form().
 *
 * Reset the filters.
 *
 * @param array $form
 *   Form representation
 * @param array $form_state
 *   Form submissions
 */
function crud_ui_form_reset($form, &$form_state) {
  unset($_SESSION['crud_ui'][$form['#crud_ui_sid']]);
}

/**
 * Reset function for crud_ui_form().
 *
 * Export the results into a CSV.
 *
 * @param array $form
 *   Form representation
 * @param array $form_state
 *   Form submissions
 */
function crud_ui_form_export($form, &$form_state) {
  $date   = format_date(time(), 'custom', 'Ymd');
  $schema = crud_ui_get_schema($form['#crud_ui_sid']);
  $length = strlen($schema['export_delimiter']);

  header('Content-Type: text/csv;');
  header('Content-Disposition: filename="export-' . $date . '.csv"');

  $output = '';
  foreach ($form['#crud_ui_cols'] as $col) {
    $field_schema = $schema['fields'][$col];

    if ((is_bool($field_schema['display_export']) && $field_schema['display_export'])
    || (is_string($field_schema['display_export']) && $field_schema['display_export']($col, $schema))) {
      $output .= $schema['export_enclosure'];
      $output .= str_replace(
        $schema['export_enclosure'],
        $schema['export_enclosure'] . $schema['export_enclosure'],
        $col
      );
      $output .= $schema['export_enclosure'];
      $output .= $schema['export_delimiter'];
    }
  }
  $output = substr($output, 0, -$length) . "\n";

  foreach ($form['#crud_ui_results'] as $record) {
    foreach ($form['#crud_ui_cols'] as $col) {
      $field_schema = $schema['fields'][$col];

      if ((is_bool($field_schema['display_export']) && $field_schema['display_export'])
      || (is_string($field_schema['display_export']) && $field_schema['display_export']($col, $schema))) {
        $theme_vars = array(
          'col'    => $col,
          'row'    => $record,
          'schema' => $schema,
        );

        $output .= $schema['export_enclosure'];
        if (!empty($field_schema['export_theme'])) {
          $value = theme($field_schema['export_theme'], $theme_vars);
        }
        else {
          $value = theme('crud_ui_text', $theme_vars);
        }

        $output .= str_replace(
          $schema['export_enclosure'],
          $schema['export_enclosure'] . $schema['export_enclosure'],
          $value
        );

        $output .= $schema['export_enclosure'];
        $output .= $schema['export_delimiter'];
      }
    }
    $output = substr($output, 0, -$length) . "\n";
  }

  echo $output;
  drupal_exit();
}
