<?php
/**
 * @file
 * This contains the view menu callbacks
 */

/**
 * Callback for menu.
 *
 * Display the detail view for a table.
 *
 * @param array $sid
 *   The ID of the CRUD UI schema
 * @param string $key
 *   Serialized primary key
 *
 * @return string
 *   HTMl output
 */
function crud_ui_admin_view($sid, $key) {
  $output = '';
  $rows   = array();
  $previous_row = array(
    'table' => '',
    'col'   => '',
  );

  $links = array(
    array(
      'text'    => crud_ui_t('Back'),
      'path'    => crud_ui_build_url_part($sid),
    ),
  );

  try {
    // Build query.
    $keys   = explode(',', $key);
    $schema = crud_ui_get_schema($sid);
    $query  = crud_ui_buid_query($schema);
    foreach ($schema['primary key'][$schema['table']] as $id => $primary_key) {
      $query->condition($schema['table'] . '.' . $primary_key, $keys[$id]);
    }
    $records = $query->execute()->fetchAll();

    foreach ($schema['fields'] as $col => $field) {
      $field_schema = $schema['fields'][$col];

      if ((is_bool($field_schema['display_view']) && $field_schema['display_view'])
      || (is_string($field_schema['display_view']) && $field_schema['display_view']($col, $schema))) {
        $rows[$col]   = array();
        $previous_col = array(
          'id'  => 0,
          'key' => '',
        );

        if ($schema['display_table_name']) {
          // Table name.
          if ($field['table'] == $previous_row['table']) {
            // Increment rowspan.
            $rows[$previous_row['col']]['table']['rowspan']++;
          }
          else {
            // Store the new previous row and add the table cell.
            $previous_row['table'] = $field['table'];
            $previous_row['col']   = $col;
            $rows[$col]['table'] = array(
              'data'    => $field['table'],
              'rowspan' => 1,
            );
          }
        }

        // Field title.
        $title = $col;
        if (!empty($field['title'])) {
          $title = $field['title'];
        }
        $rows[$col]['title'] = $title;

        foreach ($records as $id => $record) {
          // Build the uniq key for the field tables.
          $primary_key = array();
          foreach ($schema['primary key'][$field['alias']] as $value) {
            $primary_key[] = $record->$value;
          }
          $primary_key = implode(',', $primary_key);

          if ($previous_col['key'] == $primary_key && !empty($primary_key)) {
            // Increment the colspan and remove col from the current row.
            $rows[$col][$previous_col['id']]['colspan']++;
          }
          else {
            // Store the new previous col.
            $previous_col['key'] = $primary_key;
            $previous_col['id']  = $id;

            $theme_vars = array(
              'col'    => $col,
              'row'    => $record,
              'schema' => $schema,
            );

            $theme = '';
            if (!empty($field['view_theme'])) {
              $theme = theme($field['view_theme'], $theme_vars);
            }
            else {
              $theme = theme('crud_ui_text', $theme_vars);
            }

            $rows[$col][$id] = array(
              'data'    => $theme,
              'colspan' => 1,
            );
          }
        }
      }
    }

    $output = theme(
      'table',
      array(
        'rows'      => $rows,
        'empty'     => crud_ui_t('No records have been found.'),
      )
    );
  }
  catch (Exception $e) {
    watchdog('crud_ui', $e->getMessage(), array(), WATCHDOG_ERROR);
    drupal_set_message($e->getMessage(), 'error');
  }

  if ($schema['edit'] && crud_ui_access('edit', $sid)) {
    $links[] = l(
      crud_ui_t('Edit'),
      crud_ui_build_url_part($schema['sid']) . '/edit/' . $key,
      array('attributes' => array('class' => 'action-link'))
    );
  }
  if ($schema['delete'] && crud_ui_access('delete', $sid)) {
    $links[] = l(
      crud_ui_t('Delete'),
      crud_ui_build_url_part($schema['sid']) . '/delete/' . $key,
      array(
        'attributes' => array(
          'class'   => 'action-link',
          'onclick' => 'return window.confirm("' .
          crud_ui_t('Are you sure you want to delete this row ?') .
          '")',
        ),
      )
    );
  }

  return theme(
    'crud_ui_action_links',
    array('links' => $links)
  ) . $output;
}
