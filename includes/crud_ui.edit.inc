<?php
/**
 * @file
 * This contains the edit menu callbacks
 */

/**
 * Callback for menu.
 *
 * Display the row edit form for a table.
 *
 * @param array $sid
 *   The ID of the CRUD UI schema
 * @param string $key
 *   Serialized primary key
 *
 * @return string
 *   HTMl output
 */
function crud_ui_admin_edit($sid, $key) {
  drupal_add_css(drupal_get_path('module', 'crud_ui') . '/css/crud_ui.css');

  $links = array(
    array(
      'text' => crud_ui_t('Back'),
      'path' => crud_ui_build_url_part($sid),
    ),
  );

  $form = drupal_get_form(
    'crud_ui_row_form',
    array(
      'sid'    => $sid,
      'key'    => $key,
      'action' => 'edit',
    )
  );

  $schema = crud_ui_get_schema($sid);
  if ($schema['view']
   && crud_ui_access('view', $sid)) {
    $links[] = l(
      crud_ui_t('View'),
      crud_ui_build_url_part($sid) . '/view/' . $key,
      array('attributes' => array('class' => 'action-link'))
    );
  }
  if ($schema['delete']
   && crud_ui_access('delete', $sid)) {
    $links[] = l(
      crud_ui_t('Delete'),
      crud_ui_build_url_part($sid) . '/delete/' . $key,
      array(
        'attributes' => array(
          'class'   => 'action-link',
          'onclick' => 'return window.confirm("' .
          crud_ui_t('Are you sure you want to delete this row ?') .
          '")',
        ),
      )
    );
  }

  return theme(
    'crud_ui_action_links',
    array('links' => $links)
  ) . render($form);
}
