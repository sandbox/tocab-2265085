---------------------
CONTENTS OF THIS FILE
---------------------

 * Informations
 * Description
 * Requirements
 * Installation
 * Minimal usage
 * Configuration
 * Available table options
 * Available field options
 * Available types
 * Linking other tables
 * Declaring new field types
 * special type options



------------
INFORMATIONS
------------

Name: CRUD UI
Author: Tony Cabaye <tocab@smile.fr>
Drupal: 7



-----------
DESCRIPTION
-----------

This module is a developer tool that provides a hook for creating simple CRUD
(Create, Read, Update and Delete) views for your own module database tables.

For instance if you need to develop a module that need to save informations into
a specific table, you can let CRUD UI managing the basic operations for you.

It simplifies for you the management of pagination, sorting, filtering, export,
validation, access rights...etc.

Without any configuration CRUD UI is able to display consistent views based on
the drupal hook_schema declared for the table you want to explore.

It becomes even more relevant with some very simple configuration.



------------
REQUIREMENTS
------------

None.



------------
INSTALLATION
------------

Install as usual



-------------
MINIMAL USAGE
-------------

This module does nothing if you don't implement the hook_crud_ui_schema() hook
into at least one of your specific module.

The minimum configuration is :
function my_module_crud_ui_schema() {
  $schemas[] = array(
    'table' => 'my_table'
    'title' => 'my_crud_ui_name',
  );
  return $schemas;
}

After clearing the cache, it will create all CRUD views for your table in :
Reports > CRUD UI > List all rows for the database table [my_table].

And you will be able to manage the access rights for each CRUD operations for
your table in the access managing admin pages.



-------------
CONFIGURATION
-------------

You can customize the CRUD UI views by passing options like this :
function my_module_crud_ui_schema() {
  return array(
    array(
      'table'          => 'my_table_1',
      'title'          => 'My table 1',
      'table_option_1' => 'table_option_value_1',
      'table_option_2' => 'table_option_value_2',
      ...
      'fields' => array(
        'field_1' => array(
          'field_option_1' => 'field_option_value_1',
          'field_option_2' => 'field_option_value_2',
          ...
        ),
        ...
      ),
    ),
    array(
      'table'          => 'my_table_2',
      'title'          => 'My table 2',
      ...
    ),
    ...
  );
}

Your schema will be merged with tables definitions from the drupal hook_schema,
so you don't have to repeat what is already described in this hook, but you can
override it if necessary.



-----------------------
AVAILABLE TABLE OPTIONS
-----------------------

* table
  * string (required)
  * Name of the table

* title
  * string (required)
  * Title of the CRUD UI. Used for the list view menu title.

* add
  * boolean (default TRUE)
  * Displays the add action

* view
  * boolean (default TRUE)
  * Displays the detailed view action for each rows

* edit
  * boolean (default TRUE)
  * Displays the edit action for each rows

* delete
  * boolean (default TRUE)
  * Displays the delete action for each rows

* export
  * boolean (default TRUE)
  * Displays the export action

* export_enclosure
  * char (default '"')
  * CSV export enclosure

* export_delimiter
  * char (default ',')
  * CSV export delimiter

* menu_path
  * text
  * Overwrites the CRUD UI menu path for the table

* default_sort_field
  * text
  * Field name for default sorting

* default_sort_direction
  * text
  * Field name for default sorting direction
  * Use only with the default_sort_field option

* theme
  * theme name
  * Customizes the page output
  * The CRUD UI output will be available in the theme variables

* module
  * module name
  * Use this if the table you want to explore does not belong to your module

* conditions
  * array of confitions
  * Adds conditions to the select query
  * Each condition is an array containing :
    - field
    - value (default NULL)
    - operator (default '=')

* pager_options
  * array (default to : 10, 25, 50, 100, all)
  * Customize the allowed number of elements per page

* pager_default
  * pager_option key (default to the first pager option)
  * Set the default number of elements per page

* table_attributes
  * array
  * Array given to the drupal_attributes function for the table

* display_table_name
  * boolean (default FALSE)
  * Display the table name as a column in the detail view

* foreign keys
  * array of fields with options
  * See "LINKING OTHER TABLES" to learn more about foreign tables

* join
  * array of table with options
  * See "LINKING OTHER TABLES" to learn more about linked tables



-----------------------
AVAILABLE FIELD OPTIONS
-----------------------

Global field options :
* title
  * text
  * Displayed field title
  * Uses field name if empty

* type
  * text (default 'varchar')
  * Field type

* order (default to schema order multiplied by 10)
  * int
  * Display order of the field

* serialize
  * boolean (default FALSE)
  * For serialized data : unserialize the field and format it for display
  * Can also be used with the "multiple" option.
  * Can also be used with the "file" type when the file module is not enabled.

* schema_callback
  * function name
  * Customizes the field schema when processing.
  * Used to add dynamic data in the schema.


List field options :
* table
  * table name or table alias (default set to the table main option)
  * Use to display fields from linked tables (see "LINKING OTHER TABLES")

* name
  * field name (default equal to the array key)
  * Use with 'table' option when multiple fields have the same name. The array
    key of 'fileds' become the alias.

* display_list
  * boolean or function name (default TRUE)
  * Displays the field in the list view

* fictive
  * boolean (default FALSE)
  * Adds a non filterable and non sortable column
  * Must be used with the "theme" option to customize the row output

* list_theme
  * theme name
  * Customizes the list view output for the field

* filterable
  * boolean (default TRUE)
  * Allow to filter the list with the field

* filter_callback
  * function name
  * Customizes the query condition for the field when filtering
  * Not available if field is fictive or not filterable

* filter_form
  * function name
  * Customizes the filter form element for the field
  * Not available if field is fictive or not filterable

* sortable
  * boolean (default TRUE)
  * Is the field sortable ?

* col_attributes
  * CSS text
  * Array given to the drupal_attributes function for each table columns


Add and edit field options :
* display_add
  * boolean or function name (default TRUE)
  * Displays the field in the add view

* display_edit
  * boolean or function name (default TRUE)
  * Displays the field in the edit view

* default
  * text
  * Default field value

* required
  * boolean (default FALSE | TRUE if "not null"=TRUE and "default" is not set)
  * Field validation

* length
  * int
  * Maximum length of the field

* disabled
  * boolean (default FALSE | TRUE if type="serial")
  * Disable the input field

* unsigned
  * boolean (default FALSE)
  * Validation for numeric values

* precision
  * int
  * Validation for float values (total number of significant digits)

* scale
  * int
  * Validation for float values (decimal digits right of the decimal point)

* field_form
  * function name
  * Customizes the add/edit form element for the field
  * Not available if field is fictive

* validate
  * Array of function names
  * Adds a custom field validation function
  * IMPORTANT : Validate functions should be declared in you .module file.

* submit
  * function name
  * Adds a custom field submission function


View field options :
* display_view
  * boolean or function name (default TRUE)
  * Displays the field in the "view" view

* view_theme
  * theme name
  * Field theme for the view


Export field options :
* display_export
  * boolean or function name (default TRUE)
  * Displays the field in the export

* export_theme
  * theme name
  * Field theme for the export



---------------
AVAILABLE TYPES
---------------

* float        => Can be filtered by a range + float validation.

* numeric      => Can be filtered by a range + int validation.

* int          => Can be filtered by a range + int validation.

* serial       => Can be filtered by a range. Can not be set (add or edit).

* varchar      => Can be filtered with a LIKE condition.

* char         => Like varchar.

* text         => Like varchar.

* blob         => Like varchar.

* action_links => Used to add the view/edit/delete links for each row.



--------------------
LINKING OTHER TABLES
--------------------

You have two ways to do it :
1) If one of your field is a foreign key of an other table you can use the
   'foreign keys' option. It work the same as the schema API and, if it is
   already set in the table schema, you don't have to repeat it. Otherwise for
   each of your foreign fields you have to set :
   * table
     * foreign table name
     * The name of the foreign table the field is linked to

   * columns
     * array
     * Columns mapping between the main and the foreign table

   * alias
     * table alias (optional)
     * Use this if you need to add the same table multiple times in the query

   * module
     * module name (optional)
     * Use this to automaticaly load the schema of the foreign table


2) If you just want to display informations from an other table you can use the
   'join' option. For each table you want to add to the query you have to set :
   * table
     * table name
     * The name of the foreign table the field is linked to

   * condition
     * SQL text
     * The condition on which to join the table

   * alias
     * table alias (optional)
     * Use this if you need to add the same table multiple times in the query

   * module
     * module name (optional)
     * Use this to automaticaly load the schema of the linked table



-------------------------
DECLARING NEW FIELD TYPES
-------------------------

You can use the hook_crud_ui_api() hook to define new types to be used directly
in the CRUD UI schema.

Types are declared using the same field's options as in the CRUD UI schema.

They can be used to refactor some parts of the CRUD UI schema.

Look at the crud_ui_choice, crud_ui_date, crud_ui_file and crud_ui_text modules
for some examples.



--------------------
SPECIAL TYPE OPTIONS
--------------------

When declaring specific types you will be able to use some specific options :
* extends
  * type name
  * Used to retrieve automatically all options declared in any other type. You
    can override an option by declaring the option in your type definition.
