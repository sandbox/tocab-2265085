<?php
/**
 * @file
 * This file contains the CRUD UI types declaration.
 */

/**
 * Implements hook_crud_ui_api().
 */
function crud_ui_text_crud_ui_api() {
  return array(
    'types' => array(
      'email' => array(
        'validate' => array('crud_ui_text_email_validate'),
      ),
      'textarea' => array(
        'list_theme' => 'crud_ui_text_textarea',
        'view_theme' => 'crud_ui_text_textarea',
        'field_form' => 'crud_ui_text_textarea_field_form',
      ),
      'text_format' => array(
        'field_form' => 'crud_ui_text_text_format_field_form',
      ),
    ),
  );
}

/**
 * Field form element for textarea fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $default_value
 *   The field default value
 *
 * @return array
 *   Field form part
 */
function crud_ui_text_textarea_field_form($field, $schema, &$default_value) {
  return array(
    '#type'  => 'textarea',
  );
}

/**
 * Field form element for text_format fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $default_value
 *   The field default value
 *
 * @return array
 *   Field form part
 */
function crud_ui_text_text_format_field_form($field, $schema, &$default_value) {
  $form = array(
    '#type'   => 'text_format',
    '#format' => filter_default_format(),
  );

  // Modify the default value for serialized fields.
  if ($schema['fields'][$field]['serialize']) {
    $default_value = unserialize($default_value);
    if (isset($default_value['format'])) {
      $form['#format'] = $default_value['format'];
    }
    if (isset($default_value['value'])) {
      $default_value = $default_value['value'];
    }
  }

  return $form;
}
