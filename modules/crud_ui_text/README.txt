---------------------
CONTENTS OF THIS FILE
---------------------

 * Informations
 * Description
 * Requirements
 * Installation
 * Available types



------------
INFORMATIONS
------------

Name: CRUD UI text
Author: Tony Cabaye <tocab@smile.fr>
Drupal: 7



-----------
DESCRIPTION
-----------

This module extends the CRUD UI module offering advanced text types that can be
used for fields in the CRUD UI schemas.



------------
REQUIREMENTS
------------

This module depends on the crud_ui module.



------------
INSTALLATION
------------

Install as usual



---------------
AVAILABLE TYPES
---------------

* textarea     => Textarea element for the add and edit form.

* text_format  => Text-format-enabled version of the textarea element.
                  Add the "serialize" option to store both format and value in
                  the field otherwise it will use the default format.

* email        => Text input with email validation.
