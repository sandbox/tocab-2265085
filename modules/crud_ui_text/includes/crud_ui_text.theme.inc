<?php
/**
 * @file
 * This contains themes
 */

/**
 * Theme for the textarea row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_text_textarea($variables) {
  $col = $variables['col'];
  return nl2br($variables['row']->$col);
}

/**
 * Theme for the text_format row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_text_text_format($variables) {
  $col = $variables['col'];
  $value = $variables['row']->$col;
  if ($variables['schema']['fields'][$col]['serialize']) {
    $value = unserialize($value);
    if (isset($value['value']) && isset($value['format'])) {
      $value = check_markup($value['value'], $value['format']);
    }
  }
  else {
    $value = check_markup($value, filter_default_format());
  }
  return $value;
}
