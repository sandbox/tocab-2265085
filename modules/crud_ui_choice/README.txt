---------------------
CONTENTS OF THIS FILE
---------------------

 * Informations
 * Description
 * Requirements
 * Installation
 * Available field options
 * Available types



------------
INFORMATIONS
------------

Name: CRUD UI choice
Author: Tony Cabaye <tocab@smile.fr>
Drupal: 7



-----------
DESCRIPTION
-----------

This module extends the CRUD UI module offering advanced choice types
(like select) that can be used for fields in the CRUD UI schemas.



------------
REQUIREMENTS
------------

This module depends on the crud_ui module.



------------
INSTALLATION
------------

Install as usual



-----------------------
AVAILABLE FIELD OPTIONS
-----------------------

Global field options :
* choice_options
  * array
  * Defines the select options.
  * If options is not set => options will be builded up automatically with all
    distinct values existing in the database.
  * If you want continue to add new options when adding or editing a row use the
    'choice_new_option' option.
  * Use only with the 'select' type

* choice_options_translatable
  * boolean (default TRUE)
  * Define if options are translatables or not.

* choice_use_foreign_field
  * foreign field name
  * Used to display an other field instead of a basic id for example.
  * Use only with the 'select' type and the 'choice_use_foreign_table' option

* choice_use_foreign_table
  * foreign table name
  * Use only with the 'select' type and the 'choice_use_foreign_field' option


Add and edit field options :
* choice_multiple
  * boolean (default FALSE)
  * Allow multiples values for a field.
  * If the 'serialize' option is enabled, stores data as serialized array.
    Else uses the 'choice_multiple_separator'.
  * Use only with the 'select' type

* choice_multiple_separator
  * char (default ',')
  * Separator for storing values in the field.
  * Use only with the 'select' type and the 'choice_multiple' option

* choice_new_option
  * boolean (default FALSE)
  * Add a textfield input on the add/edit form.
  * Use only with the 'select' type

* choice_create_only
  * boolean (default FALSE)
  * Fill the author field only when adding a row (not when editing).
  * Use only with the 'author' type




---------------
AVAILABLE TYPES
---------------

* boolean => Yes/No select form element for filtering, adding or editing.
             Can look better when setting the width with the 'col_attributes'
             option.

* select  => Select form element for filtering. Can look better when setting the
             width with the "col_attributes" option.
