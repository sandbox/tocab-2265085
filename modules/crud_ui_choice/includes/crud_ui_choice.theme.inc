<?php
/**
 * @file
 * This contains themes
 */

/**
 * Theme for the select row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_choice_select($variables) {
  $output       = '';
  $col          = $variables['col'];
  $value        = $variables['row']->$col;
  $field_schema = $variables['schema']['fields'][$col];

  if (!empty($field_schema['choice_multiple'])) {
    $values = explode(
      $field_schema['choice_multiple_separator'],
      $variables['row']->$col
    );

    $output .= '<ul>';
    foreach ($values as $value) {
      if (isset($field_schema['choice_options'][$value])) {
        $output .= '<li>' . $field_schema['choice_options'][$value] . '</li>';
      }
      else {
        $output .= '<li>' . $value . '</li>';
      }
    }
    $output .= '</ul>';
  }
  else {
    if (isset($field_schema['choice_options'][$value])) {
      $output .= $field_schema['choice_options'][$value];
    }
    else {
      $output .= $value;
    }
  }

  return $output;
}
