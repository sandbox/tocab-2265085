<?php
/**
 * @file
 * This file contains the CRUD UI types declaration.
 */

/**
 * Implements hook_crud_ui_api().
 */
function crud_ui_choice_crud_ui_api() {
  return array(
    'types' => array(
      'select' => array(
        'filter_form'     => 'crud_ui_choice_select_filter_form',
        'filter_callback' => 'crud_ui_choice_select_filter_callback',
        'list_theme'      => 'crud_ui_choice_select',
        'export_theme'    => 'crud_ui_choice_select',
        'view_theme'      => 'crud_ui_choice_select',
        'field_form'      => 'crud_ui_choice_select_field_form',
        'submit'          => 'crud_ui_choice_select_submit',
        'schema_callback' => 'crud_ui_choice_select_schema_callback',
      ),
      'boolean' => array(
        'extends'        => 'select',
        'choice_options' => array(
          '0' => 'No',
          '1' => 'Yes',
        ),
      ),
      'author' => array(
        'extends'                     => 'select',
        'disabled'                    => TRUE,
        'submit'                      => 'crud_ui_choice_author_submit',
        'choice_options_translatable' => FALSE,
        'choice_use_foreign_field'    => 'name',
        'choice_use_foreign_table'    => 'users',
      ),
    ),

    'field_option_default_values' => array(
      'choice_options_translatable' => TRUE,
      'choice_multiple'             => FALSE,
      'choice_multiple_separator'   => ',',
      'choice_new_option'           => FALSE,
      'choice_create_only'          => FALSE,
    ),
  );
}

/**
 * Filter form element for boolean fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return array
 *   Field form part
 */
function crud_ui_choice_select_filter_form($field, $schema, $values) {
  return array(
    '#type'    => 'select',
    '#title'   => '',
    '#options' => crud_ui_choice_build_options($schema['fields'][$field]),
  );
}

/**
 * Add the query condition for the select element.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   User values
 * @param SelectQuery $select
 *   Select query object
 */
function crud_ui_choice_select_filter_callback($field, $schema, $values, $select) {
  $field_schema = $schema['fields'][$field];

  if (isset($values[$field]) && $values[$field] !== '') {
    if (!empty($field_schema['choice_multiple'])) {
      $select->condition(
        $field_schema['full_name'],
        '%' . $values[$field] . '%',
        'LIKE'
      );
    }
    else {
      $select->condition(
        $field_schema['full_name'],
        $values[$field]
      );
    }
  }
}

/**
 * Field form element for select fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $default_value
 *   The field default value
 *
 * @return array
 *   Field form part
 */
function crud_ui_choice_select_field_form($field, &$schema, &$default_value) {
  $field_schema = $schema['fields'][$field];
  $options      = crud_ui_choice_build_options($field_schema);

  if (!empty($field_schema['choice_multiple'])) {
    // Remove the length option because it is not compatible with the multiple
    // selector.
    unset($field_schema['length']);

    if ($field_schema['serialize']) {
      $default_value = unserialize($default_value);
    }
    else {
      $default_value = explode(
        $field_schema['choice_multiple_separator'],
        $default_value
      );
    }
  }

  if (isset($field_schema['choice_new_option'])
   && $field_schema['choice_new_option']) {
    $form = array(
      '#type'                => 'fieldset',
      '#collapsible'         => TRUE,
      '#collapsed'           => FALSE,
      $field                 => array(
        '#type'          => 'select',
        '#title'         => crud_ui_t('Select existing option'),
        '#options'       => $options,
        '#multiple'      => !empty($field_schema['choice_multiple']),
        '#default_value' => $default_value,
      ),
      $field . '_new_option' => array(
        '#type'        => 'textfield',
        '#title'       => crud_ui_t('Or add new option'),
        '#description' => crud_ui_t('Overrides the selection if not empty'),
      ),
    );
  }
  else {
    $form = array(
      '#type'     => 'select',
      '#options'  => $options,
      '#multiple' => !empty($field_schema['choice_multiple']),
    );
  }

  return $form;
}

/**
 * Submit function for select fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return string
 *   The value to be stored in the field
 */
function crud_ui_choice_select_submit($field, $schema, $values) {
  $value        = NULL;
  $field_schema = $schema['fields'][$field];

  if (!empty($field_schema['choice_new_option'])
   && !empty($values[$field . '_new_option'])) {
    $value = $values[$field . '_new_option'];
    // Clear the schema cache to refresh options for automatic selectors.
    cache_clear_all('crud_ui_schemas', 'cache');
  }
  elseif (!empty($field_schema['choice_multiple'])) {
    if ($field_schema['serialize']) {
      $value = serialize($values[$field]);
    }
    else {
      $value = implode(
        $field_schema['choice_multiple_separator'],
        $values[$field]
      );
    }
  }
  else {
    $value = crud_ui_default_value($field, $field_schema, $values);
  }

  return $value;
}

/**
 * Fill select options if empty during schema processing.
 *
 * @param array $field
 *   The field name
 * @param array $field_schema
 *   The field schema
 *
 * @return array
 *   Field form part
 */
function crud_ui_choice_select_schema_callback($field, &$field_schema) {
  if (empty($field_schema['choice_options'])) {
    $table       = $field_schema['table'];
    $label_field = $field;

    if (!empty($field_schema['choice_use_foreign_field'])) {
      $table       = $field_schema['choice_use_foreign_table'];
      $label_field = $field_schema['choice_use_foreign_field'];
    }

    $field_schema['choice_options'] = db_select($table)
      ->fields($table, array($field, $label_field))
      ->distinct()
      ->execute()
      ->fetchAllKeyed();
  }
}

/**
 * Submit function for author fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return string
 *   The value to be stored in the field
 */
function crud_ui_choice_author_submit($field, $schema, $values) {
  global $user;
  $value = NULL;

  if (is_object($user)
   && property_exists($user, 'uid')
   && !empty($user->uid)
   && ($values['op'] == crud_ui_t('Add') || empty($schema['fields'][$field]['choice_create_only']))) {
    $value = $user->uid;
  }

  return $value;
}

/**
 * Build the selector options.
 *
 * @param array $field_schema
 *   The field schema
 *
 * @return array
 *   Selector options
 */
function crud_ui_choice_build_options($field_schema) {
  $options = array('' => '');
  foreach ($field_schema['choice_options'] as $value => $label) {
    if ($field_schema['choice_options_translatable']) {
      $options[$value] = crud_ui_t($label);
    }
    else {
      $options[$value] = $label;
    }
  }
  return $options;
}
