<?php
/**
 * @file
 * This file contains the CRUD UI types declaration.
 */

/**
 * Implements hook_crud_ui_api().
 */
function crud_ui_file_crud_ui_api() {
  return array(
    'types' => array(
      'file' => array(
        'list_theme'   => 'crud_ui_file_file',
        'export_theme' => 'crud_ui_file_file_export',
        'view_theme'   => 'crud_ui_file_file',
        'field_form'   => 'crud_ui_file_file_field_form',
        'submit'       => 'crud_ui_file_file_submit',
        'filterable'   => FALSE,
      ),
    ),

    'field_option_default_values' => array(
      'file_status'          => FILE_STATUS_PERMANENT,
      'file_upload_location' => 'public://',
    ),
  );
}

/**
 * Field form element for file fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $default_value
 *   The field default value
 *
 * @return array
 *   Field form part
 */
function crud_ui_file_file_field_form($field, $schema, &$default_value) {
  $field_schema      = $schema['fields'][$field];
  $upload_location   = $field_schema['file_upload_location'];
  $upload_validators = array();

  if (!empty($field_schema['file_upload_validators'])) {
    $upload_validators = $field_schema['file_upload_validators'];
  }

  return array(
    '#type'              => 'managed_file',
    '#upload_location'   => $upload_location,
    '#upload_validators' => $upload_validators,
  );
}

/**
 * Submit function for file fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return string
 *   The value to be stored in the field
 */
function crud_ui_file_file_submit($field, $schema, $values) {
  $file = file_load($values[$field]);
  $file->status = $schema['fields'][$field]['file_status'];
  file_save($file);

  return $values[$field];
}
