<?php
/**
 * @file
 * This contains themes
 */

/**
 * Theme for the file row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_file_file($variables) {
  $col  = $variables['col'];
  $output = '';

  if (!empty($variables['row']->$col)) {
    $output = theme('file_link', array('file' => file_load($variables['row']->$col)));
  }

  return $output;
}

/**
 * Theme for the file row in the export.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_file_file_export($variables) {
  $col  = $variables['col'];
  $output = '';

  if (!empty($variables['row']->$col)) {
    $file   = file_load($variables['row']->$col);
    $output = file_create_url($file->uri);
  }

  return $output;
}
