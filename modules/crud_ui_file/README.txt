---------------------
CONTENTS OF THIS FILE
---------------------

 * Informations
 * Description
 * Requirements
 * Installation
 * Available field options
 * Available types



------------
INFORMATIONS
------------

Name: CRUD UI file
Author: Tony Cabaye <tocab@smile.fr>
Drupal: 7



-----------
DESCRIPTION
-----------

This module extends the CRUD UI module offering file types that can be used for
fields in the CRUD UI schemas.



------------
REQUIREMENTS
------------

This module depends on the crud_ui and the file module.



------------
INSTALLATION
------------

Install as usual



-----------------------
AVAILABLE FIELD OPTIONS
-----------------------

Add and edit field options :
* file_status
  * file status (default FILE_STATUS_PERMANENT)
  * Drupal file status for internal files managing.

* file_upload_location
  * location (default 'public://')
  * The file upload location

* file_upload_validators
  * array
  * The file upload validators



---------------
AVAILABLE TYPES
---------------

* file => Upload file managed with the file module.
