---------------------
CONTENTS OF THIS FILE
---------------------

 * Informations
 * Description
 * Requirements
 * Installation
 * Available field options
 * Available types



------------
INFORMATIONS
------------

Name: CRUD UI date
Author: Tony Cabaye <tocab@smile.fr>
Drupal: 7



-----------
DESCRIPTION
-----------

This module extends the CRUD UI module offering date types that can be used for
fields in the CRUD UI schemas.



------------
REQUIREMENTS
------------

This module depends on the crud_ui and date_popup module.



------------
INSTALLATION
------------

Install as usual



-----------------------
AVAILABLE FIELD OPTIONS
-----------------------

Global field options :
* date_format
  * text (default 'd/m/Y H:i:s')
  * PHP date format


Add and edit field options :
* date_create_only
  * boolean (default FALSE)
  * Fill the date_auto field only when adding a row (not when editing).
  * Use only with the 'date_auto' type



---------------
AVAILABLE TYPES
---------------

* date       => Use it on your timestamp fields with the date_format option.

* date_popup => Like date with a datepicker.

* data_auto  => Like date but can not be added or edited. The timestamp is set
                automatically when a new row is added or edited.
