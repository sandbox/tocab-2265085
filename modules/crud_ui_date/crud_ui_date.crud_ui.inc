<?php
/**
 * @file
 * This file contains the CRUD UI types declaration.
 */

/**
 * Implements hook_crud_ui_api().
 */
function crud_ui_date_crud_ui_api() {
  return array(
    'types' => array(
      'date' => array(
        'filter_form'     => 'crud_ui_date_date_filter_form',
        'filter_callback' => 'crud_ui_date_date_filter_callback',
        'list_theme'      => 'crud_ui_date_format_date',
        'export_theme'    => 'crud_ui_date_format_date',
        'view_theme'      => 'crud_ui_date_format_date',
        'field_form'      => 'crud_ui_date_date_field_form',
        'submit'          => 'crud_ui_date_date_submit',
      ),
      'date_auto' => array(
        'extends'  => 'date',
        'disabled' => TRUE,
        'submit'   => 'crud_ui_date_creation_date_submit',
      ),
      'date_popup' => array(
        'extends'     => 'date',
        'filter_form' => 'crud_ui_date_date_popup_filter_form',
        'field_form'  => 'crud_ui_date_date_popup_field_form',
      ),
    ),

    'field_option_default_values' => array(
      'date_format'      => CRUD_UI_DATE_DEFAULT_DATE_FORMAT,
      'date_create_only' => FALSE,
    ),
  );
}

/**
 * Filter form element for date fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return array
 *   Field form part
 */
function crud_ui_date_date_filter_form($field, $schema, $values) {
  $format = $schema['fields'][$field]['date_format'];

  $form = array();
  $form[$field . '-from'] = array(
    '#type'        => 'textfield',
    '#title'       => crud_ui_t('from :'),
    '#size'        => NULL,
    '#attributes' => array(
      'placeholder' => $format,
    ),
  );
  $form[$field . '-to'] = array(
    '#type'        => 'textfield',
    '#title'       => crud_ui_t('to :'),
    '#size'        => NULL,
    '#attributes' => array(
      'placeholder' => $format,
    ),
  );

  if (!empty($values[$field . '-from'])) {
    $form[$field . '-from']['#default_value'] = $values[$field . '-from'];
  }
  if (!empty($values[$field . '-to'])) {
    $form[$field . '-to']['#default_value'] = $values[$field . '-to'];
  }

  return $form;;
}

/**
 * Add the query condition for the date fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   User values
 * @param SelectQuery $select
 *   Select query object
 */
function crud_ui_date_date_filter_callback($field, $schema, $values, $select) {
  $field_schema = $schema['fields'][$field];
  $format       = $field_schema['date_format'];

  if (!empty($values[$field . '-from'])) {
    $date = new DateObject($values[$field . '-from'], NULL, $format);
    $select->condition(
        $field_schema['full_name'],
        $date->getTimestamp(),
        '>='
    );
  }
  if (!empty($values[$field . '-to'])) {
    $date = new DateObject($values[$field . '-to'], NULL, $format);
    $select->condition(
        $field_schema['full_name'],
        $date->getTimestamp(),
        '<='
    );
  }
}

/**
 * Field form element for date fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $default_value
 *   The field default value
 *
 * @return array
 *   Field form part
 */
function crud_ui_date_date_field_form($field, $schema, &$default_value) {
  if (!empty($default_value)) {
    $date = new DateObject($default_value);
    $default_value = $date->format($schema['fields'][$field]['date_format']);
  }

  return array(
    '#type' => 'textfield',
  );
}

/**
 * Submit function for date fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return string
 *   The value to be stored in the field
 */
function crud_ui_date_date_submit($field, $schema, $values) {
  $date = new DateObject(
    $values[$field],
    NULL,
    $schema['fields'][$field]['date_format']
  );
  return $date->getTimestamp();
}

/**
 * Submit function for date_auto fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return string
 *   The value to be stored in the field
 */
function crud_ui_date_creation_date_submit($field, $schema, $values) {
  $value  = NULL;
  if ($values['op'] == crud_ui_t('Add') || empty($schema['fields'][$field]['date_create_only'])) {
    $date  = new DateObject();
    $value = $date->getTimestamp();
  }
  return $value;
}

/**
 * Filter form element for date_popup fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $values
 *   Input values
 *
 * @return array
 *   Field form part
 */
function crud_ui_date_date_popup_filter_form($field, $schema, $values) {
  $format = $schema['fields'][$field]['date_format'];

  $form = array();
  $form[$field . '-from'] = array(
    '#type'        => 'date_popup',
    '#title'       => crud_ui_t('from :'),
    '#date_format' => $format,
    '#size'        => NULL,
    '#attributes'  => array(
      'placeholder' => $format,
    ),
  );
  $form[$field . '-to'] = array(
    '#type'        => 'date_popup',
    '#title'       => crud_ui_t('to :'),
    '#date_format' => $format,
    '#size'        => NULL,
    '#attributes'  => array(
      'placeholder' => $format,
    ),
  );

  if (!empty($values[$field . '-from'])) {
    $form[$field . '-from']['#default_value'] = $values[$field . '-from'];
  }
  if (!empty($values[$field . '-to'])) {
    $form[$field . '-to']['#default_value'] = $values[$field . '-to'];
  }

  return $form;
}

/**
 * Field form element for date_popup fields.
 *
 * @param array $field
 *   The field name
 * @param array $schema
 *   The CRUD UI schema
 * @param array $default_value
 *   The field default value
 *
 * @return array
 *   Field form part
 */
function crud_ui_date_date_popup_field_form($field, $schema, &$default_value) {
  crud_ui_date_date_field_form($field, $schema, $default_value);
  return array(
    '#type'        => 'date_popup',
    '#date_format' => $schema['fields'][$field]['date_format'],
  );
}
