<?php
/**
 * @file
 * This contains themes
 */

/**
 * Theme for the date row.
 *
 * @param array $variables
 *   Array of variables
 *
 * @return string
 *   HTML output
 */
function theme_crud_ui_date_format_date($variables) {
  $col = $variables['col'];
  $output = $variables['row']->$col;

  return format_date(
    $output,
    'custom',
    $variables['schema']['fields'][$col]['date_format']
  );
}
