---------------------
CONTENTS OF THIS FILE
---------------------

 * Informations
 * Description
 * Requirements
 * Installation
 * Example 1
 * Example 2
 * Example 3
 * Example 4
 * Example 5



------------
INFORMATIONS
------------

Name: CRUD UI example
Author: Tony Cabaye <tocab@smile.fr>
Drupal: 7



-----------
DESCRIPTION
-----------

This module is an example module for developers of how to use the crud_ui
module.

Each CRUD UI implementation show differents features.
Please look at crud_ui_example.crud_ui.inc and
crud_ui_example.info files.



------------
REQUIREMENTS
------------

None.



------------
INSTALLATION
------------

Install as usual



---------
EXAMPLE 1
---------

The first example show the minimum configuration required for the hook_crud_ui
to work.
The table "crud_ui_example" is declared in the hook_schema of the .install file
and all of the following configuration are also handled by the hook_crud_ui
(look at the README.txt of the crud_ui module to see theirs effects in the
differents CRUD views) :

  * 'serial' type
  * 'int' type
  * 'varchar' type
  * 'float' type
  * 'text' type

  * Table option 'table' (required in hook_crud_ui)
  * Table option 'name'  (required in hook_crud_ui)
  * Field option 'unsigned'
  * Field option 'not null'
  * Field option 'description'
  * Field option 'length'
  * Field option 'precision'
  * Field option 'scale'
  * Field option 'default'



---------
EXAMPLE 2
---------

Based on the same table as the first example, the CRUD views are nevertheless
quite different.
This example show how the advanced following configuration can be implemented :

  * 'select' type       (from crud_ui_choice module)
  * 'email' type        (from crud_ui_text module)
  * 'boolean' type      (from crud_ui_choice module)
  * 'textarea' type     (from crud_ui_text module)
  * 'date' type         (from crud_ui_date module)
  * 'action_links' type

  * Table option 'default_sort_field'
  * Table option 'default_sort_direction'
  * Table option 'theme'

  * Field option 'display_edit'
  * Field option 'order'
  * Field option 'choice_new_option' (for the 'select' type)
  * Field option 'filter_form'
  * Field option 'filter_callback'
  * Field option 'disabled'
  * Field option 'display_add'
  * Field option 'required'
  * Field option 'validate'
  * Field option 'filterable'
  * Field option 'sortable'
  * Field option 'date_format' (for the 'date' type)



---------
EXAMPLE 3
---------

This example is based on the node table.
It shows that you can create the CRUD views for tables that does not directly
belong to your module.
So be extremely careful using this features.
Here we disable the add, edit, delete and export views to keep only the list.
In facts it shows how the following configuration can be implemented :

  * 'date_popup' type

  * Table option 'export'
  * Table option 'add'
  * Table option 'view'
  * Table option 'edit'
  * Table option 'delete'
  * Table option 'module'
  * Table option 'conditions'
  * Table option 'foreign keys'

  * Field option 'title'
  * Field option 'display_list'
  * Field option 'choice_options_translatable' (for the 'select' type)
  * Field option 'choice_use_foreign_field'    (for the 'select' type)
  * Field option 'choice_use_foreign_table'    (for the 'select' type)
  * Field option 'table'
  * Field option 'name'
  * Field option 'fictive'
  * Field option 'list_theme'

  * Foreign keys option 'table'
  * Foreign keys option 'columns'



---------
EXAMPLE 4
---------

This example shows :

  * 'file' type (from crud_ui_file module)

  * Table option 'pager_options'
  * Table option 'pager_default'

  * Field option 'display_export'
  * Field option 'display_view'
  * Field option 'file_upload_location'   (for the 'file' type)
  * Field option 'file_upload_validators' (for the 'file' type)
  * Field option 'export_theme'
  * Field option 'view_theme'



---------
EXAMPLE 5
---------

This example shows :

  * 'author' type      (from crud_ui_choice module)
  * 'text_format' type (from crud_ui_text module)
  * 'date_auto' type   (from crud_ui_auto module)

  * Table option 'menu_path'
  * Table option 'table_attributes'
  * Table option 'display_table_name'
  * Table option 'join'

  * Field option 'choice_create_only'        (for the 'author' type)
  * Field option 'choice_options'            (for the 'select' type)
  * Field option 'choice_multiple'           (for the 'select' type)
  * Field option 'choice_multiple_separator' (for the 'select' type)
  * Field option 'col_attributes'
  * Field option 'date_create_only'          (for the 'date' type)

  * Join option 'table'
  * Join option 'alias'
  * Join option 'condition'
  * Join option 'module'
