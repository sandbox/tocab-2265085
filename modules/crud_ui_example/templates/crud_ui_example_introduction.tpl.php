<?php
/**
 * @file
 * Add an introduction for the crud_ui second example.
 *
 * Available variables:
 * - $crud_ui: The standard crud_ui output.
 *
 * @ingroup themeable
 */

?>
<h2><?php print crud_ui_t('Welcome to the crud_ui second example.'); ?></h2>
<p><?php print crud_ui_t('Here you can see all entries from the table @table.', array('@table' => 'crud_ui_example')); ?></p>
<p><?php print crud_ui_t('But they are presented a different way from the first example.'); ?></p>
<?php print $crud_ui; ?>
