<?php
/**
 * @file
 * This module shows examples for the CRUD UI module
 */

/**
 * Implements hook_crud_ui_schema().
 */
function crud_ui_example_crud_ui_schema() {
  $schemas = array();

  /********** First example **********/
  $schemas[] = array(
    // Minimim usage.
    'table' => 'crud_ui_example',
    'title' => 'CRUD UI example 1',
  );

  /********** Second example **********/
  $schemas[] = array(
    'table'                  => 'crud_ui_example',
    'title'                  => 'CRUD UI example 2',
    // Sort the list view from the oldest to the youngest.
    'default_sort_field'     => 'age',
    'default_sort_direction' => 'desc',
    // Theme the list view for adding an extra introduction.
    'theme'                  => 'crud_ui_example_introduction',
    // Specific export options.
    'export_enclosure'       => '"',
    'export_delimiter'       => ';',
    'fields' => array(
      'id' => array(
        // We don't want to display this field in the edit view.
        'display_edit' => FALSE,
      ),
      'civility' => array(
        // Rearrange the field.
        // According to the schema, uid order = 20 and name order = 30.
        'order'             => 25,
        // We want the civility to be presented as a select element.
        'type'              => 'select',
        // Not very relevant here but we want to have the possibility to add new
        // civility when adding or editing a row.
        'choice_new_option' => TRUE,
        // We don't specify the options to get them automatically.
        // This only works if you already have data stored.
      ),
      'email' => array(
        // 'email' type is a textfield element with e-mail validation.
        'type'            => 'email',
        // For the example we modified the way the field is filtered in the
        // list view.
        'filter_form'     => 'crud_ui_example_email_filter_form',
        'filter_callback' => 'crud_ui_example_email_filter_callback',
      ),
      'valide' => array(
        // Rearrange the fields.
        // According to the schema, the size order = 60.
        'order'       => 55,
        // Accept only true or false.
        'type'        => 'boolean',
        // When we create a new entries the e-mail has not been validated yet.
        'display_add' => FALSE,
      ),
      'size' => array(
        // The size cannot be set with this CRUD UI form.
        'disabled' => TRUE,
      ),
      'age' => array(
        // Required field.
        'required' => TRUE,
        // We do not allow age greater than 200 years old...
        'validate' => array('crud_ui_example_age_validate'),
      ),
      'address' => array(
        // We allow multiple lines for the address.
        'type'       => 'textarea',
        // So it is not very relevant to allow filtering and sorting on this
        // field.
        'filterable' => FALSE,
        'sortable'   => FALSE,
      ),
      'created' => array(
        // Format the date.
        'type'        => 'date',
        'date_format' => 'm/d/Y',
      ),
      'changed' => array(
        'type'        => 'date',
        'date_format' => 'm/d/Y',
      ),
      // Add action links to view, edit and delete the row.
      'action_links' => array(
        'type' => 'action_links',
      ),
    ),
  );

  /********** Third example **********/
  $schemas[] = array(
    'table'  => 'node',
    'title'  => 'CRUD UI example 3',
    // Do not allow other views that the list view.
    'export' => FALSE,
    'add'    => FALSE,
    'view'   => FALSE,
    'edit'   => FALSE,
    'delete' => FALSE,
    // We want to display a table that is not declared in our module.
    'module' => 'node',
    // List only published nodes.
    'conditions' => array(
      array('node.status', 1),
    ),
    'fields' => array(
      'nid' => array(
        // Give a human readable title to the field.
        'title' => 'Node ID (nid)',
      ),
      'vid' => array(
        // We don't want this field to be displayed in the list view.
        'display_list' => FALSE,
      ),
      'type' => array(
        // 'select' type without options : used for smart filtering.
        'type'                        => 'select',
        'title'                       => 'Node type',
        // Node types do not need to be translated.
        'choice_options_translatable' => FALSE,
      ),
      'language' => array(
        'type'  => 'select',
        'title' => 'Node language',
      ),
      'title' => array(
        'title' => 'Node title',
      ),
      'uid' => array(
        'type'                        => 'select',
        'title'                       => 'Author',
        // Use the name field of the user table instead of a basic id.
        'choice_options_translatable' => FALSE,
        'choice_use_foreign_field'    => 'name',
        'choice_use_foreign_table'    => 'users',
      ),
      'mail' => array(
        'title' => 'Author email',
        // This field comes from the foreign 'users' table.
        'table' => 'users',
        'order' => 65,
      ),
      'status' => array(
        'display_list' => FALSE,
      ),
      'created' => array(
        // Use the date_popup type for easy filtering.
        'type'        => 'date_popup',
        'date_format' => 'm/d/Y',
        'title'       => 'Node creation time',
      ),
      'changed' => array(
        'type'        => 'date_popup',
        'date_format' => 'm/d/Y',
        'title'       => 'Node update time',
      ),
      'comment' => array(
        'display_list' => FALSE,
      ),
      'promote' => array(
        'type'  => 'boolean',
        'title' => 'Node is promoted ?',
      ),
      'sticky' => array(
        'type'  => 'boolean',
        'title' => 'Node is sticky ?',
      ),
      'tnid' => array(
        'display_list' => FALSE,
      ),
      'translate' => array(
        'display_list' => FALSE,
      ),
      'user_created' => array(
        'type'        => 'date_popup',
        'date_format' => 'm/d/Y',
        'title'       => 'Author creation time',
        'table'       => 'users',
        // As the 'created' field already exists in the 'node' table we should
        // use an alias for the key (user_created) and set the real field name
        // in the 'name' option (as to be used with the 'table' option).
        'name'        => 'created',
      ),
      'links' => array(
        // Fictive column : add a column to the list view that is not a table
        // field. We use it to display the node links.
        'fictive'    => TRUE,
        // Theme the fictive column to display node links.
        'list_theme' => 'crud_ui_example_row_node',
        'title'      => 'Action',
      ),
    ),
  );

  /********** Fourth example **********/
  $schemas[] = array(
    'table'         => 'crud_ui_example_file',
    'title'         => 'CRUD UI example 4',
    'pager_options' => array(
      '5'  => '5',
      '10' => '10',
      '25' => '25',
    ),
    'pager_default' => '10',
    'fields' => array(
      'id' => array(
        // We don't want to display this field in the export.
        'display_export' => FALSE,
        // Nor in the detailed view.
        'display_view' => FALSE,
      ),
      'file' => array(
        // 'file' type using the file module.
        'type'                   => 'file',
        'file_upload_location'   => 'public://crud_ui_example',
        'file_upload_validators' => array(
          'file_validate_extensions' => array('png jpg jpeg gif'),
        ),
        // We don't want to display the file name, but the fid in the export.
        'export_theme'           => 'crud_ui_example_file_export',
        // We want to display the image in the detailed view if the file module
        // is enabled.
        'view_theme'             => 'crud_ui_example_file_view',
      ),
      'action_links' => array(
        'type' => 'action_links',
      ),
    ),
  );

  /********** Fith example **********/
  $schemas[] = array(
    'table'              => 'crud_ui_example',
    'title'              => 'CRUD UI example 5',
    // We want to change the menu path for the views.
    'menu_path'          => 'admin/structure/crud_ui_example_5',
    // As we have lot of information to display we want a big table.
    'table_attributes'   => array('style' => 'min-width: 1500px;'),
    // With multiples tables, better display each table name in the detail view.
    'display_table_name' => TRUE,
    'fields' => array(
      'uid' => array(
        // This type allow to save the author automatically.
        'type'               => 'author',
        'title'              => 'Author',
        'choice_create_only' => TRUE,
      ),
      'civility' => array(
        'type'                      => 'select',
        // We specify the civility options.
        'choice_options'            => array(
          'Mr.'  => 'Mister',
          'Mrs.' => 'Madam',
          'Ms.'  => 'Miss',
        ),
        // For the example we want here a multiple select.
        'choice_multiple'           => TRUE,
        'choice_multiple_separator' => '|',
      ),
      'email' => array(
        'display_list' => FALSE,
      ),
      'size' => array(
        'display_list' => FALSE,
      ),
      'age' => array(
        'display_list' => FALSE,
      ),
      'address' => array(
        // The address can be set in HTML format.
        'type'           => 'text_format',
        // Force the size of the column to be a little bigger.
        'col_attributes' => array('style' => 'min-width: 20%;'),
      ),
      'valide' => array(
        'display_list' => FALSE,
      ),
      'created' => array(
        // Use this type to save the date automatically.
        'type'             => 'date_auto',
        'date_format'      => 'm/d/Y',
        // But fill the field only when a new row is created (not edited).
        'date_create_only' => TRUE,
      ),
      'changed' => array(
        // Here update on both case (add or edit).
        'type'        => 'date_auto',
        'date_format' => 'm/d/Y',
      ),
      'action_links' => array(
        'type'  => 'action_links',
        // As this field is not a database field, we must force the order.
        'order' => 115,
      ),
      // As the field name 'id' already exists in the crud_ui_example table we
      // define here an alias for this field (same for other fields below).
      'order_id' => array(
        'table' => 'crud_ui_example_order',
        // Or : 'table' => 'cueo',
        'name'  => 'id',
      ),
      // Alias of crud_ui_example_order.pid.
      'person_id' => array(
        'display_list' => FALSE,
        'display_view' => FALSE,
        'table'        => 'crud_ui_example_order',
        'name'         => 'pid',
      ),
      // Alias of crud_ui_example_order_item.oid.
      'order_id' => array(
        'display_list' => FALSE,
        'display_view' => FALSE,
        'table'        => 'crud_ui_example_order_item',
        'name'         => 'oid',
      ),
      // Alias of crud_ui_example_order_item.pid.
      'product_id' => array(
        'table' => 'crud_ui_example_order_item',
        'name'  => 'pid',
      ),
      'title' => array(
        'type'  => 'select',
        'table' => 'crud_ui_example_product',
      ),
      // Alias of crud_ui_example_product.id.
      'crud_ui_example_product_id' => array(
        'display_list' => FALSE,
        'display_view' => FALSE,
        'table'        => 'crud_ui_example_product',
        'name'         => 'id',
      ),
      // Alias of crud_ui_example_product.uid.
      'crud_ui_example_product_uid' => array(
        'display_list' => FALSE,
        'display_view' => FALSE,
        'table'        => 'crud_ui_example_product',
        'name'         => 'uid',
      ),
    ),
    'join' => array(
      array(
        'table'     => 'crud_ui_example_order',
        // Not necessary here but we set a table alias.
        'alias'     => 'cueo',
        'condition' => 'crud_ui_example.id = cueo.pid',
        // We have to add the 'module' option if we want the schema of the table
        // to be automatically loaded.
        'module'    => 'crud_ui_example',
      ),
      array(
        'table'     => 'crud_ui_example_order_item',
        'alias'     => 'cueoi',
        'condition' => 'cueo.id = cueoi.oid',
        'module'    => 'crud_ui_example',
      ),
    ),
    'foreign keys' => array(
      array(
        'table'  => 'crud_ui_example_product',
        'module' => 'crud_ui_example',
      ),
    ),
  );

  return $schemas;
}
